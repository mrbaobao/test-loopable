import { make } from 'loopable'
import type { NextPage } from 'next'

const Home: NextPage = () => {
  const arrUsers = [
    { name: "Iron Man" },
    { name: "Black Widow" },
    { name: "Captain America" },
    { name: "Peter Parker" },
  ]
  //Log initial
  console.log(1, arrUsers)
  //Make it loopable
  const loopableList = make(arrUsers)
  //Add modifier
  loopableList.add((item, index) => {
    item.name = "I am " + item.name
  })
  //Add modifier
  loopableList.add((item, index) => {
    item.age = 28 - index * 2
  })
  //Loop one time
  const output = loopableList.loop((item, index) => {
    item.is_woman = index == 1
    return <div key={index}>{index + 1}. {item.name}, {item.age}, {item.is_woman ? "woman" : "man"}</div>
  })
  console.log(2, arrUsers)
  return (
    <section>
      {output}
    </section>
  )
}

export default Home
